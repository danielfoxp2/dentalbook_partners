defmodule DentalbookPartners.PartnerControllerTest do
  use DentalbookPartners.ConnCase

  alias DentalbookPartners.Partner
  @valid_attrs %{cellphone: "some content", cpf: "some content", name: "some content"}
  @invalid_attrs %{}

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, partner_path(conn, :index)
    assert html_response(conn, 200) =~ "Listing partners"
  end

  test "renders form for new resources", %{conn: conn} do
    conn = get conn, partner_path(conn, :new)
    assert html_response(conn, 200) =~ "New partner"
  end

  test "creates resource and redirects when data is valid", %{conn: conn} do
    conn = post conn, partner_path(conn, :create), partner: @valid_attrs
    assert redirected_to(conn) == partner_path(conn, :index)
    assert Repo.get_by(Partner, @valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, partner_path(conn, :create), partner: @invalid_attrs
    assert html_response(conn, 200) =~ "New partner"
  end

  test "shows chosen resource", %{conn: conn} do
    partner = Repo.insert! %Partner{}
    conn = get conn, partner_path(conn, :show, partner)
    assert html_response(conn, 200) =~ "Show partner"
  end

  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, partner_path(conn, :show, -1)
    end
  end

  test "renders form for editing chosen resource", %{conn: conn} do
    partner = Repo.insert! %Partner{}
    conn = get conn, partner_path(conn, :edit, partner)
    assert html_response(conn, 200) =~ "Edit partner"
  end

  test "updates chosen resource and redirects when data is valid", %{conn: conn} do
    partner = Repo.insert! %Partner{}
    conn = put conn, partner_path(conn, :update, partner), partner: @valid_attrs
    assert redirected_to(conn) == partner_path(conn, :show, partner)
    assert Repo.get_by(Partner, @valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    partner = Repo.insert! %Partner{}
    conn = put conn, partner_path(conn, :update, partner), partner: @invalid_attrs
    assert html_response(conn, 200) =~ "Edit partner"
  end

  test "deletes chosen resource", %{conn: conn} do
    partner = Repo.insert! %Partner{}
    conn = delete conn, partner_path(conn, :delete, partner)
    assert redirected_to(conn) == partner_path(conn, :index)
    refute Repo.get(Partner, partner.id)
  end
end
