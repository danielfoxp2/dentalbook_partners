defmodule DentalbookPartners.PartnerTest do
  use DentalbookPartners.ModelCase

  alias DentalbookPartners.Partner

  @valid_attrs %{cellphone: "some content", cpf: "some content", name: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Partner.changeset(%Partner{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Partner.changeset(%Partner{}, @invalid_attrs)
    refute changeset.valid?
  end
end
