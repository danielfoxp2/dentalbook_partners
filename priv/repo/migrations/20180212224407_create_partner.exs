defmodule DentalbookPartners.Repo.Migrations.CreatePartner do
  use Ecto.Migration

  def change do
    create table(:partners) do
      add :name, :string
      add :cellphone, :string
      add :cpf_cnpj, :string
      add :email, :string
      add :market_link, :string
      
      # authenticatable
      add :password_hash, :string
      # recoverable
      add :reset_password_token, :string
      add :reset_password_sent_at, :utc_datetime
      # lockable
      add :failed_attempts, :integer, default: 0
      add :locked_at, :utc_datetime
      # trackable
      add :sign_in_count, :integer, default: 0
      add :current_sign_in_at, :utc_datetime
      add :last_sign_in_at, :utc_datetime
      add :current_sign_in_ip, :string
      add :last_sign_in_ip, :string
      # unlockable_with_token
      add :unlock_token, :string

      # bank account to be paid
      add :bank_id, :string
      add :bank, :string
      add :account_owner_document, :string
      add :account, :string
      add :digit_account, :string
      add :agency, :string
      add :account_type, :string

      timestamps()
    end
    create unique_index(:partners, [:email])
    create unique_index(:partners, [:cpf_cnpj])

  end
end
