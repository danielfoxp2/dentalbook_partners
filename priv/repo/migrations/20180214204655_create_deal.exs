defmodule DentalbookPartners.Repo.Migrations.CreateDeal do
  use Ecto.Migration

  def change do
    create table(:deals, primary_key: false) do
      add :partner_id, :integer
      add :content, :binary
      
      timestamps()
    end
  
  end
end
