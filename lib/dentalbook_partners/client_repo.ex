defmodule DentalbookPartners.ClientRepo do
  alias DentalbookPartners.Repo
  alias DentalbookPartners.ClientQuery
  alias DentalbookPartners.WageCalculator

  def get_achieved_clients_of(partner_id) do
    partner_id
    |> get_query_of_active_clients_achieved_to
    |> Repo.query
    |> parse_to_map
  end

  def get_all_data_of_this(company_id) do
    company_id
    |> get_query_of_all_data_of
    |> Repo.query
    |> parse_to_map(:only_one)
  end

  defp get_query_of_active_clients_achieved_to(partner_id) do
    ClientQuery.build(partner_id)
  end

  defp get_query_of_all_data_of(company_id) do
    """
    select 
      cmp.id, 
      cmp.name empresa, 
      us.name usuario, 
      us.cellphone,
      us.email,
      ac.id, 
      ac.description plano, 
      ac.price
    from companies cmp
    inner join company_users cpu
    on cmp.id = cpu.company_id
    inner join users us
    on us.id = cpu.user_id
    inner join account_plans ac
    on ac.id = cmp.account_plan_id
    where 
      cmp.id = #{company_id}
    and
      us.cellphone is not null
    """
  end

  defp parse_to_map({_, company}, :only_one) do
    company
    |> get_rows_data
    |> transform_in_map(:only_one)
  end
  
  defp parse_to_map({_, companies_achieved}) do
    companies_achieved
    |> get_rows_data
    |> transform_in_map
    |> split_by_status    
    |> calculate_full_payment
    |> return_parsed_data
  end

  defp get_rows_data(%Postgrex.Result{rows: rows}), do: rows
  defp get_rows_data(_), do: []

  defp transform_in_map(these_rows, :only_one) do
    Enum.map(these_rows, fn row_values -> 
      get_client_data_from(row_values, :only_one)
    end)
    |> List.first
  end

  defp transform_in_map(these_rows) do
    Enum.map(these_rows, fn row_values -> 
      { get_client_payment_status_from(row_values), get_client_data_from(row_values)}
    end)
  end

  defp get_client_payment_status_from(query_result) do
    Enum.at(query_result, 0)
    |> String.to_atom
  end

  defp get_client_data_from(query_result, :only_one) do
    %{
      company_id: Enum.at(query_result, 0),
      company_name: Enum.at(query_result, 1),
      owner_name: Enum.at(query_result, 2),
      owner_cellphone: Enum.at(query_result, 3),
      owner_email: Enum.at(query_result, 4),
      account_plan_id: Enum.at(query_result, 5),
      account_plan: Enum.at(query_result, 6),
      account_price: Enum.at(query_result, 7) |> Money.new |> Money.to_string
    }
  end

  defp get_client_data_from(query_result) do
    %{
      company_id: Enum.at(query_result, 1),
      company_name: Enum.at(query_result, 2),
      owner_name: Enum.at(query_result, 3),
      owner_cellphone: Enum.at(query_result, 4),
      account_plan_id: Enum.at(query_result, 5),
      account_plan: Enum.at(query_result, 6),
      partner_payment: get_adjusted_payment(query_result)
    }
  end

  defp get_adjusted_payment(query_result) do
    company_status = get_client_payment_status_from(query_result)
    account_plan_id = Enum.at(query_result, 5)

    Enum.at(query_result, 7)
    |> WageCalculator.adjustment_payment(company_status, account_plan_id)
  end

  defp split_by_status(clients) do
    %{
      pendents: filter(:pendent, clients), 
      actives_full: filter(:active_full, clients), 
      actives_percentual: filter(:active_percentual, clients),
      actives_full_semester: filter(:active_full_semester, clients),
      actives_percentual_semester: filter(:active_percentual_semester, clients),
      inactives: filter(:inactive, clients)
    }
  end

  defp filter(searched_status, of_clients) do
    Enum.filter(of_clients, fn {status, _} -> status == searched_status end)
    |> Keyword.get_values(searched_status)
  end

  defp calculate_full_payment(clients) do
    full_partner_payment = get_full_partner_payment(clients)

    {full_partner_payment, clients}
  end

  defp get_full_partner_payment(params) do
    Money.new(0)
    |> add_total_of(:actives_full, params)
    |> add_total_of(:actives_percentual, params)
    |> add_total_of(:actives_full_semester, params)
    |> add_total_of(:actives_percentual_semester, params)
  end

  defp add_total_of(acumulator, active_key, params) do
    active_clients = Map.get(params, active_key)

    Enum.reduce(active_clients, acumulator, fn(achieved_client, partial_total) -> 
      Money.add(achieved_client.partner_payment, partial_total) 
    end)
  end

  defp return_parsed_data({full_partner_payment, clients}) do
    [
      partner_payment: full_partner_payment, 
      pendent_clients: Map.get(clients, :pendents),
      active_clients: get_all_active_of(clients),
      inactive_clients: Map.get(clients, :inactives)
    ] 
  end

  defp get_all_active_of(clients) do
    Map.get(clients, :actives_full) ++
    Map.get(clients, :actives_percentual) ++
    Map.get(clients, :actives_full_semester) ++
    Map.get(clients, :actives_percentual_semester)
  end

end