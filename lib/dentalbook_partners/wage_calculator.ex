defmodule DentalbookPartners.WageCalculator do
  
  @payment_percentage 0.3
  @accountPlansIdsOf180Days [290335]

  def adjustment_payment(value, :pendent, account_plan_id)
  when account_plan_id in @accountPlansIdsOf180Days, do: calculate_molar_fee(value)
  def adjustment_payment(value, :pendent, _), do: value |> Money.new 
 
  def adjustment_payment(value, :active_full, _), do: value |> Money.new 
  def adjustment_payment(value, :active_full_semester, account_plan_id)
  when account_plan_id in @accountPlansIdsOf180Days, do: calculate_molar_fee(value)

  def adjustment_payment(value, _, _) do
    value
    |> Money.new 
    |> Money.multiply(@payment_percentage)
  end

  defp calculate_molar_fee(value) do
    paid_months = 6
    
    first_month_payment = value 
    |> Money.new 
    |> Money.divide(paid_months)
    |> List.first
    
    value 
    |> Money.new 
    |> Money.subtract(first_month_payment)
    |> Money.multiply(@payment_percentage)
    |> Money.add(first_month_payment)
  end


 


end