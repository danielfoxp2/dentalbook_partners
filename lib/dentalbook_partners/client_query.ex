defmodule DentalbookPartners.ClientQuery do
  alias DentalbookPartners.Filter
  
  def build(partner_id) do
    """
    #{select("pendent")}
    #{from_companies()}
    #{Filter.where_is_pendent_of(partner_id)}

    union

    #{select("active_full")}
    #{from_companies()}
    #{Filter.where_is_active_full_of(partner_id)}

    union

    #{select("active_percentual")}
    #{from_companies()}
    #{Filter.where_is_active_percentual_of(partner_id)}

    union

    #{select("active_full_semester")}
    #{from_companies()}
    #{Filter.where_is_active_full_semester_of(partner_id)}

    union

    #{select("active_percentual_semester")}
    #{from_companies()}
    #{Filter.where_is_active_percentual_semester_of(partner_id)}

    union 

    #{select("inactive")}
    #{from_companies()}
    #{Filter.where_is_inactive_of(partner_id)}

    """
  end

  defp select(client_status) do
  """
    select 
    '#{client_status}' status,
    cmp.id, 
    cmp.name empresa, 
    us.name usuario, 
    us.cellphone, 
    ac.id, 
    ac.description plano, 
    ac.price
  """
  end

  defp from_companies() do
  """
    from companies cmp
    inner join company_users cpu
    on cmp.id = cpu.company_id
    inner join users us
    on us.id = cpu.user_id
    inner join account_plans ac
    on ac.id = cmp.account_plan_id  
  """
  end
end