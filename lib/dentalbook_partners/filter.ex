defmodule DentalbookPartners.Filter do

  def where_is_pendent_of(partner_id) do
  """
    where 
      cellphone is not null
    and 
      (select count(*) from monthly_company_payments mpay
       where mpay.company_id = cmp.id) = 1
    and (select extract(day from (current_timestamp - 
         (select min(mpay2.start_access) 
          from monthly_company_payments mpay2
          where mpay2.company_id = cmp.id)))) <= 30
    and exists 
      (select 1 from monthly_company_payments mpay
       where mpay.company_id = cmp.id
       and mpay.end_access >= current_timestamp)
    and cmp.partner_id = #{partner_id}
  """ 
  end

  def where_is_active_full_of(partner_id) do
  """
    where 
      cellphone is not null
    and 
      (select count(*) from monthly_company_payments mpay
       where mpay.company_id = cmp.id
       and extract(day from (mpay.end_access - mpay.start_access)) <= 30) = 2
    and 
      (select max(mpay.end_access) from monthly_company_payments mpay
       where mpay.company_id = cmp.id) >= current_timestamp
    and cmp.partner_id = #{partner_id}  
  """
  end

  def where_is_active_percentual_of(partner_id) do
  """
    where 
      cellphone is not null
    and 
      (select count(*) from monthly_company_payments mpay
       where mpay.company_id = cmp.id) > 2
    and 
      (select max(mpay.end_access) from monthly_company_payments mpay
       where mpay.company_id = cmp.id) >= current_timestamp
    and cmp.partner_id = #{partner_id}  
  """
  end

  def where_is_active_full_semester_of(partner_id) do
  """
    where 
      cellphone is not null
    and 
      (select count(*) from monthly_company_payments mpay
       where mpay.company_id = cmp.id    
       and extract(day from (mpay.end_access - mpay.start_access)) > 30) = 1
    and 
      (select extract(day from (current_timestamp - 
                (select min(mpay2.start_access) 
                 from monthly_company_payments mpay2
                 where mpay2.company_id = cmp.id)))) > 30    
    and 
      (select extract(day from (current_timestamp - 
                (select min(mpay2.start_access) 
                 from monthly_company_payments mpay2
                 where mpay2.company_id = cmp.id)))) <= 60
    and 
      (select max(mpay.end_access) from monthly_company_payments mpay
       where mpay.company_id = cmp.id) >= current_timestamp
    and cmp.partner_id = #{partner_id}  
  """
  end

  def where_is_active_percentual_semester_of(partner_id) do
  """
    where 
      cellphone is not null
    and 
      (select count(*) from monthly_company_payments mpay
       where mpay.company_id = cmp.id
       and extract(day from (mpay.end_access - mpay.start_access)) > 30) > 1
    and (select extract(day from (current_timestamp - 
         (select max(mpay2.start_access) 
         from monthly_company_payments mpay2
         where mpay2.company_id = cmp.id)))) <= 30
    and 
      (select max(mpay.end_access) from monthly_company_payments mpay
       where mpay.company_id = cmp.id) >= current_timestamp
    and cmp.partner_id = #{partner_id}  
  """
  end
  
  def where_is_inactive_of(partner_id) do
  """  
    where 
      cellphone is not null
    and 
      (select count(*) from monthly_company_payments mpay
       where mpay.company_id = cmp.id) > 1
    and 
      (select max(mpay.end_access) from monthly_company_payments mpay
       where mpay.company_id = cmp.id) < current_timestamp
    and cmp.partner_id = #{partner_id}
  """
  end

end