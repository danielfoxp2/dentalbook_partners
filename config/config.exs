# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :dentalbook_partners,
  ecto_repos: [DentalbookPartners.Repo]

# Configures the endpoint
config :dentalbook_partners, DentalbookPartners.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "tv2yq+VPgsfWWnvgSX11lslDMyMQnU2pwchqUhStzm762yGj/E6UZJsrlZwgbY2h",
  render_errors: [view: DentalbookPartners.ErrorView, accepts: ~w(html json)],
  pubsub: [name: DentalbookPartners.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"

# %% Coherence Configuration %%   Don't remove this line
config :coherence,
  user_schema: DentalbookPartners.Partner,
  repo: DentalbookPartners.Repo,
  module: DentalbookPartners,
  router: DentalbookPartners.Router,
  messages_backend: DentalbookPartners.Coherence.Messages,
  logged_out_url: "/",
  email_from_name: "Your Name",
  email_from_email: "yourname@example.com",
  opts: [:authenticatable, :recoverable, :lockable, :trackable, :unlockable_with_token, :registerable]

config :coherence, DentalbookPartners.Coherence.Mailer,
  adapter: Swoosh.Adapters.Sendgrid,
  api_key: "your api key here"
# %% End Coherence Configuration %%

  config :money,
  default_currency: :BRL,
  separator: ".",
  delimeter: ",",
  symbol: true,
  symbol_on_right: false,
  symbol_space: true

