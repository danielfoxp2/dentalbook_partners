use Mix.Config

# For development, we disable any cache and enable
# debugging and code reloading.
#
# The watchers configuration can be used to run external
# watchers to your application. For example, we use it
# with brunch.io to recompile .js and .css sources.
config :dentalbook_partners, DentalbookPartners.Endpoint,
  http: [port: 4001],
  debug_errors: true,
  code_reloader: true,
  check_origin: false,
  watchers: [node: ["node_modules/brunch/bin/brunch", "watch", "--stdin",
                    cd: Path.expand("../", __DIR__)]]


# Watch static and templates for browser reloading.
config :dentalbook_partners, DentalbookPartners.Endpoint,
  live_reload: [
    patterns: [
      ~r{priv/static/.*(js|css|png|jpeg|jpg|gif|svg)$},
      ~r{priv/gettext/.*(po)$},
      ~r{web/views/.*(ex)$},
      ~r{web/templates/.*(eex)$}
    ]
  ]

# Do not include metadata nor timestamps in development logs
config :logger, :console, format: "[$level] $message\n"

# Set a higher stacktrace during development. Avoid configuring such
# in production as building large stacktraces may be expensive.
config :phoenix, :stacktrace_depth, 20

# Configure your database
config :dentalbook_partners, DentalbookPartners.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: System.get_env("CASHBOOK_POSTGRES_ENV_POSTGRES_USER"),
  password: System.get_env("CASHBOOK_POSTGRES_ENV_POSTGRES_PASSWORD"),
  database: "dental_book_dev",
  hostname: System.get_env("CASHBOOK_POSTGRES_PORT_5432_TCP_ADDR"),
  pool_size: 10
