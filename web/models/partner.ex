defmodule DentalbookPartners.Partner do
  use DentalbookPartners.Web, :model
  use Coherence.Schema

  schema "partners" do
    field :name, :string
    field :cellphone, :string
    field :cpf_cnpj, :string
    field :email, :string
    field :bank_id, :string
    field :bank, :string
    field :account_owner_document, :string
    field :account, :string
    field :digit_account, :string
    field :agency, :string
    field :account_type, :string
    field :market_link, :string

    coherence_schema()

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """

  def changeset(struct, params \\ %{})

  def changeset(struct, %{"bank_id" => _bank_id} = params) do
    required_fields = [:bank_id, :bank, :account_owner_document, :account, :agency, :account_type]
    casted_fields = [:digit_account] ++ required_fields

    struct
    |> cast(params, casted_fields)
    |> validate_required(required_fields)
  end

  def changeset(struct, %{"market_link" => market_link} = params) do
    struct
    |> cast(params, [:market_link])
  end

  def changeset(struct, params) do
    struct
    |> cast(params, [:name, :cellphone, :cpf_cnpj, :email, :market_link] ++ coherence_fields())
    |> validate_required([:name, :cellphone, :cpf_cnpj, :email])
    |> validate_format(:email, ~r/@/)
    |> unique_constraint(:email)
    |> unique_constraint(:cpf_cnpj)
    |> validate_coherence(params)
  end
  
  def changeset(model, params, :password) do
    model
    |> cast(params, ~w(password password_confirmation reset_password_token reset_password_sent_at))
    |> validate_coherence_password_reset(params)
  end
end
