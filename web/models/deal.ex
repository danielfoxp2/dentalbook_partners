defmodule DentalbookPartners.Deal do
  use DentalbookPartners.Web, :model

  @primary_key false
  schema "deals" do
    field :partner_id, :integer
    field :content, :string
    
    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:partner_id, :content])
    |> validate_required([:partner_id, :content])
  end
end