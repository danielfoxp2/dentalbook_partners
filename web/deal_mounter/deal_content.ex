defmodule DentalbookPartners.DealMounter.DealContent do

  def fill_for(partner) do
    %{partner_id: partner.id, content: prepare_clauses_of_the_deal_of(partner)}
  end

  defp prepare_clauses_of_the_deal_of(partner) do
    partner
    |> prepare_intro
    |> prepare_clause_one(partner)
    |> prepare_clause_two
    |> prepare_clause_three
    |> prepare_clause_four
    |> prepare_clause_five
    |> prepare_clause_six
    |> prepare_clause_seven
    |> prepare_clause_eight
    |> prepare_clause_nine
    |> prepare_clause_ten
    |> prepare_clause_eleven
    |> prepare_clause_twelve
    |> prepare_clause_thirteen
    |> prepare_clause_fourteen
    |> prepare_clause_fifteen
    |> prepare_clause_sixteen
    |> prepare_clause_seventeen
  end

  defp prepare_intro(with_this_partner) do
    """
    <p>
      Pelo presente instrumento particular de contrato de representação comercial, e devidamente assinado eletronicamente
      ao realizar o aceite dos termos apresentados, a firma 7S Representações, devidamente inscrita no CNPJ/MF sob o 
      nº 28.661.710/0001-81 (“Dentalbook”), gestora do software Dentalbook, acessado pelo site https://www.dentalbook.com.br 
      e seus subdomínios, neste ato representada por seu sócio proprietário, devidamente qualificado através de sua assinatura 
      digital doravante designada "REPRESENTADA", e, de outro lado, #{with_this_partner.name} representante comercial registrado 
      sob o número de CPF/CNPJ: #{with_this_partner.cpf_cnpj} doravante designado (a) "REPRESENTANTE", sujeitando-se às normas da 
      Lei número 4.886, de 09/12/65, com as alterações introduzidas pela Lei nº 8.420 de 08/05/92, têm entre si, 
      justo e contratado, o que mutuamente aceitam nas cláusulas abaixo estipuladas:
    </p>
    """
  end

  defp prepare_clause_one(previous_clauses, with_this_partner) do
    """
    #{previous_clauses}

    <p>
      <b>PRIMEIRA</b></br></br>

      A REPRESENTADA, por força do presente ajuste, nomeia #{with_this_partner.name} seu representante, para fins
      de comercialização do acesso ao software Dentalbook (www.dentalbook.com.br), de propriedade da REPRESENTADA.
    </p>
    """
  end

  defp prepare_clause_two(previous_clauses) do
    """
    #{previous_clauses}

    <p>
      <b>SEGUNDA</b></br></br>
      
      Cabe ao REPRESENTANTE, como primordial obrigação, a promoção de vendas, dos artigos, produtos e serviços, 
      objetos de comércio da REPRESENTADA, agenciando propostas, transmitindo-as para aceitação e fidelizando
      os clientes.
    </p>
    """
  end

  defp prepare_clause_three(previous_clauses) do
    """
    #{previous_clauses}

    <p>
    <b>TERCEIRA</b></br></br>

      O REPRESENTANTE fará jus à comissão, pelos negócios que realizar diretamente ou por intermédio 
      de terceiros que o represente, por força do presente contrato.
    </p>
    """
  end

  defp prepare_clause_four(previous_clauses) do
    """
    #{previous_clauses}

    <p>
      <b>QUARTA</b></br></br>

      O REPRESENTANTE poderá exercer sua atividade para outra empresa, ou efetuar negócios em nome e por conta própria, 
      desde que não se trate de atividade concorrente com a REPRESENTADA.
    </p>
    """
  end

  defp prepare_clause_five(previous_clauses) do
    """
    #{previous_clauses}

    <p>
      <b>QUINTA</b></br></br>

      O REPRESENTANTE fica obrigado a fornecer à REPRESENTADA, quando lhe for solicitado, informações detalhadas 
      sobre o andamento dos negócios postos a seu cargo, devendo dedicar-se à representação de modo a expandir os 
      negócios da REPRESENTADA e promover seus produtos.
    </p>
    """
  end

  defp prepare_clause_six(previous_clauses) do
    """
    #{previous_clauses}

    <p>
      <b>SEXTA</b></br></br>

      Salvo autorização expressa, não poderá o REPRESENTANTE conceder abatimento, descontos ou dilações de prazo, 
      nem agir em desacordo com as instruções da REPRESENTADA.
    </p>
    """
  end

  defp prepare_clause_seven(previous_clauses) do
    """
    #{previous_clauses}

    <p>
      <b>SÉTIMA</b></br></br>

      O REPRESENTANTE poderá ser constituído mandatário, com poderes especiais para conclusão de negócios e, 
      além dos deveres gerais emergentes deste contrato, deverá agir na estrita conformidade do mandato que lhe 
      for outorgado, ficando sujeito às prescrições legais relativas ao mandato mercantil.
    </p>
    """
  end

  defp prepare_clause_eight(previous_clauses) do
    """
    #{previous_clauses}

    <p>
      <b>OITAVA</b></br></br>

      Não serão prejudicados os direitos do REPRESENTANTE, quando a título de cooperação, desempenhe temporariamente, 
      a pedido da REPRESENTADA, encargos ou atribuições diversas dos previstos no presente contrato.
    </p>
    """
  end

  defp prepare_clause_nine(previous_clauses) do
    """
    #{previous_clauses}

    <p>
      <b>NONA</b></br></br>

      A REPRESENTADA se reserva ao direito de não estabelecer quaisquer acordo de exclusividade com o REPRESENTANTE
      referente a comercialização em zonas. Estando a REPRESENTADA isenta de quaisquer ônus relacionado à esta matéria.
    </p>
    """
  end

  defp prepare_clause_ten(previous_clauses) do
    """
    #{previous_clauses}

    <p>
      <b>DÉCIMA</b></br></br>

      O REPRESENTANTE, a título de retribuição, receberá sobre as vendas realizadas por seu intermédio 
      o valor integral da primeira mensalidade do cliente. 
      Ainda sobre a remuneração, o REPRESENTANTE receberá 30% de comissão sobre o valor padrão do plano escolhido 
      pelo cliente a partir de sua segunda mensalidade, enquanto o cliente estiver com sua conta ativa no 
      software Dentalbook e o REPRESENTANTE mantiver relacionamento com a REPRESENTADA. A forma de remuneração
      respeitará as seguintes condições para cada plano:
    </p>
    <ul>
      <li>
        Planos INCISIVO e CANINO: o representante recebe pelo valor padrão do plano, independente da quantidade de usuários
        que sejam criados pelo comprador.  
      </li>
      </br>
      <li>
        Plano MOLAR: O valor pago pelo cliente é dividido em 6 parcelas, você recebe o valor correspondente a 1º parcela 
        mais 30% das 5 parcelas restantes. De uma única vez.
      </li>
    </ul>
    """
  end

  defp prepare_clause_eleven(previous_clauses) do
    """
    #{previous_clauses}

    <p>
      <b>DÉCIMA PRIMEIRA</b></br></br>

      O REPRESENTANTE, somente recebe os valores descritos na DÉCIMA cláusula, na conclusão do pagamento da segunda
      mensalidade pelo cliente. Caso o cliente desative sua conta no software Dentalbook da REPRESENTADA no prazo de 
      até 30 dias, a contar da criação de sua conta, o REPRESENTANTE não receberá valor algum a título de retribuição
      pela venda realizada, uma vez que, com o cancelamento dentro do prazo estipulado, o cliente tem o direito de receber 
      o estorno integral do valor pago na primeira mensalidade. 
    </p>
    """
  end

  defp prepare_clause_twelve(previous_clauses) do
    """
    #{previous_clauses}

    <p>
      <b>DÉCIMA SEGUNDA</b></br></br>

      O REPRESENTANTE poderá exigir as comissões a si devidas, respeitando as cláusulas DÉCIMA e DÉCIMA PRIMEIRA deste 
      termo e em acordo com a REPRESENTADA que definirá e informará ao REPRESENTANTE a data de deposito de comissões.
    </p>
    """
  end

  defp prepare_clause_thirteen(previous_clauses) do
    """
    #{previous_clauses}

    <p>
      <b>DÉCIMA TERCEIRA</b></br></br>

      Nenhuma retribuição será devida ao REPRESENTANTE, se a falta do pagamento resultar de insolvência do comprador, 
      bem como se o negócio vir a ser por ele desfeito, ou for sustada a entrega do acesso ao software Dentalbook
      por ser duvidosa a liquidação.
    </p>
    """
  end

  defp prepare_clause_fourteen(previous_clauses) do
    """
    #{previous_clauses}

    <p>
      <b>DÉCIMA QUARTA</b></br></br>

      As despesas necessárias ao exercício normal da representação ora concedida, relativas a transporte, hospedagem, 
      selos, telegramas, mostruário, propaganda, etc., correm por conta do REPRESENTANTE.
    </p>
    """
  end

  defp prepare_clause_fifteen(previous_clauses) do
    """
    #{previous_clauses}

    <p>
      <b>DÉCIMA QUINTA</b></br></br>

      A rescisão, sem motivo, do presente contrato pela REPRESENTADA, fora dos casos previstos no art. 35 da Lei nº 4.886/65, 
      dará ao REPRESENTANTE o direito ao recebimento do proporcional do mês vigente da mensalidade do cliente em acordo com 
      as cláusulas DÉCIMA, DÉCIMA PRIMEIRA e DÉCIMA SEGUNDA.
    </p>
    """
  end

  defp prepare_clause_sixteen(previous_clauses) do
    """
    #{previous_clauses}

    <p>
      <b>DÉCIMA SEXTA</b></br></br>

      A REPRESENTADA poderá alterar, a qualquer tempo, as condições do presente contrato, visando seu aprimoramento e 
      melhoria dos serviços prestados. As novas condições entrarão em vigor 15 (quinze) dias após a notificação ao 
      REPRESENTANTE, que será realizada via email. Sendo assim, caso o REPRESENTANTE não concorde com as modificações 
      publicadas, este deverá imediatamente proceder com o cancelamento de sua representação via email junto a REPRESENTADA.
      Neste caso, o vínculo contratual do REPRESENTANTE com a REPRESENTADA deixará de existir. 
      No entanto, não havendo manifestação no prazo acima estipulado, entender-se-á que o REPRESENTANTE aceitou 
      tacitamente as novas condições do contrato e continuará vinculando as partes.
    </p>
    """
  end

  defp prepare_clause_seventeen(previous_clauses) do
    """
    #{previous_clauses}

    <p>
      <b>DÉCIMA SÉTIMA</b></br></br>

      O prazo de duração do presente contrato é indeterminado, entrando em vigor na data de seu aceite eletrônico. 
      E por estarem justos e contratados, REPRESENTADA e REPRESENTANTE, firmam o presente e concordam 
      integralmente com essas disposições, se comprometendo a respeitar as condições aqui previstas de forma 
      irretratável e irrevogável. Reconhecem que o presente termo se formaliza, vinculando as Partes, com a sua 
      aceitação eletrônica pelo REPRESENTANTE, o que se fará mediante o clique na opção “Sim! Aceito os termos”;            
    </p>
    """
  end
end