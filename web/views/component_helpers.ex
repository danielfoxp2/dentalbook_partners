defmodule DentalbookPartners.ComponentHelpers do
  
  alias DentalbookPartners.ComponentView

  def c(template, assigns) do
    ComponentView.render(template, assigns)
  end

  def c(template, assigns, do: block) do
    ComponentView.render(template, Keyword.merge(assigns, [do: block]))
  end
end