defmodule DentalbookPartners.PartnerView do
  use DentalbookPartners.Web, :view

  def format_currency(value), do: Money.to_string(value)
  
end
