defmodule DentalbookPartners.DealView do
  use DentalbookPartners.Web, :view
  alias DentalbookPartners.DealMounter.DealContent

  def show_filled_deal(partner) do
    DealContent.fill_for(partner)
    |> get_filled()
  end
  
  defp get_filled(%{content: content}), do: raw(content)
end