defmodule DentalbookPartners.PartnerController do
  use DentalbookPartners.Web, :controller

  alias DentalbookPartners.Partner
  alias DentalbookPartners.ClientRepo

  def index(conn, _params) do
    partner = conn.assigns.current_user
    assigns = ClientRepo.get_achieved_clients_of(partner.id)

    render(conn, "index.html", market_link_sufix: partner.market_link, assigns: assigns)
  end

  def new(conn, _params) do
    changeset = Partner.changeset(%Partner{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"partner" => partner_params}) do
    changeset = Partner.changeset(%Partner{}, partner_params)

    case Repo.insert(changeset) do
      {:ok, _partner} ->
        conn
        |> put_flash(:info, "Partner created successfully.")
        |> redirect(to: partner_path(conn, :index))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    partner = Repo.get!(Partner, id)
    render(conn, "show.html", partner: partner)
  end

  def edit(conn, %{"id" => id}) do
    partner = Repo.get!(Partner, id)
    changeset = Partner.changeset(partner)
    render(conn, "edit.html", partner: partner, changeset: changeset)
  end

  def update(conn, %{"id" => id, "partner" => partner_params}) do
    partner = Repo.get!(Partner, id)

    partner_params = Map.put(partner_params, "bank_id", get_bank_id(partner_params["bank"]))
    changeset = Partner.changeset(partner, partner_params)

    case Repo.update(changeset) do
      {:ok, partner} ->
        conn
        |> put_flash(:info, "Partner updated successfully.")
        |> redirect(to: registration_path(conn, :show, user: partner))
      {:error, changeset} ->
        render(conn, "edit.html", partner: partner, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    partner = Repo.get!(Partner, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(partner)

    conn
    |> put_flash(:info, "Partner deleted successfully.")
    |> redirect(to: partner_path(conn, :index))
  end

  defp get_bank_id(bank_name) do
    bank_name
    |> String.trim
    |> String.split("-")
    |> List.first
  end
end
