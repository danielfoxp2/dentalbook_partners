defmodule DentalbookPartners.DealController do
  use DentalbookPartners.Web, :controller
  alias DentalbookPartners.DealMounter.DealContent
  alias DentalbookPartners.Deal
  alias DentalbookPartners.Repo
  alias DentalbookPartners.Partner
  
  def show(conn, params) do
    IO.puts "representante"
    IO.inspect params
    partner = Repo.get_by(Partner, email: params["email"])
    render conn, "deal.html", partner: partner
  end

  def create(conn, params) do
    IO.puts "Salva o contrato no banco"
    IO.puts "Envia email do contrato para as partes"
    IO.puts "Redireciona para a tela de login"

    IO.inspect params

    params
    |> get_partner
    |> create_market_link
    |> fill_deal_content_to
    |> save

    conn
    |> put_flash(:info, "Sua conta foi criada com sucesso. Informe os dados abaixo para acessar sua área privativa.")
    |> redirect(to: session_path(conn, :new))
  end

  defp get_partner(%{"partner_id" => partner_id}), do: Repo.get(Partner, partner_id)

  defp create_market_link(partner) do
    partner
    |> get_splited_full_name
    |> make_market_link_of(partner)
    |> do_update(partner)
  end

  defp fill_deal_content_to(partner), do: DealContent.fill_for(partner)

  defp save(deal_content) do
    %Deal{}
    |> Deal.changeset(deal_content)
    |> Repo.insert
  end

  defp get_splited_full_name(partner) do 
    String.split(partner.name, " ") 
    |> Enum.reject(fn name_piece -> name_piece == "" end)
  end

  defp make_market_link_of(splited_name, of_partner) do
    name_for_link = Enum.count(splited_name) |> get_name_from(splited_name)

    "#{name_for_link}-#{of_partner.id}"
  end

  defp do_update(market_link, of_partner) do
    changes = %{"market_link" => market_link}
    of_partner
    |> Partner.changeset(changes)
    |> Repo.update

    of_partner
  end

  defp get_name_from(number_of_elements_in_name, splited_name) when number_of_elements_in_name == 1 do
    List.first(splited_name)
    |> String.downcase
  end

  defp get_name_from(_number_of_elements_in_name, splited_name) do
    "#{List.first(splited_name)}-#{List.last(splited_name)}"
    |> String.downcase
  end

end
  