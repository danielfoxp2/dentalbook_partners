defmodule DentalbookPartners.PageController do
  use DentalbookPartners.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
