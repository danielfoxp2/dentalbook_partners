defmodule DentalbookPartners.CompanyController do
  use DentalbookPartners.Web, :controller
  alias DentalbookPartners.ClientRepo
  alias DentalbookPartners.Repo

  def show(conn, %{"company_id" => company_id}) do
    client_details = ClientRepo.get_all_data_of_this(company_id)
    render(conn, "show.html", client_details: client_details)
  end

end